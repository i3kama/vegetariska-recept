# Chili på svarta bönor

Hittade det här receptet på [Jonas sida](https://gist.github.com/jonasryberg/2752dd195795371ea431ee4a2cf543e4#file-chili_pa_svarta_bonor-md)

Ingredienser
------------
- 500 gram svarta bönor (blötläggs i minst 8 timmar)
- 3 msk olja 
- 4 vitlöksklyftor, hackade 
- Två röda paprikor, hackade i ganska grova bitar
- 2 röda lökar, hackade 
- 1/2 msk salt  
- 2 matskedar rökt paprikapulver 
- 1 matsked mald spiskummin
- 1 matsked tomatpuré  
- 1 chipotlefrukt
- 6 dl vatten 
- 1 burk krossad tomat
- 1 burk majs

Blötlägg bönorna. När de är klara, häll av vattnet och värm oljan i tryckkokaren på mediumvärme. Lägg i lök, paprika, vitlök och salt. Rör runt då och då tills gränsakerna mjuknat. Lägg i spiskummin och paprikapulver. Förtsätt röra i två minuter. Läg i den hackade chipotlen och tomatpurén. Häll på vattnet, de krossade tomaterna, bönorna och ev mer salt. 

Sätt sedan locket på tryckkokaren. När den nått högsta tryck, sänk temperaturen och låt koka i 30 minuter. Efter 30 minuter, ta kokaren av plattan och låt den svalna i ytterligare 15 minuter. Släpp sedan ut eventuellt kvarvarande tryck. Smaka av med mer salt om det behövs. Häll i majsen och låt chillin koka upp igen. 

Servera med lime, gräddfil, tacochips eller annat som passar.  